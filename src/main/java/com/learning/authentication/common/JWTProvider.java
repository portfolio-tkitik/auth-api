package com.learning.authentication.common;

import static java.util.stream.Collectors.joining;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.learning.authentication.common.configuration.AuthorizeConfiguration;

// import com.learning.authentication.common.configuration.AuthorizeConfiguration;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class JWTProvider {

    @NonNull
    private final AuthorizeConfiguration configuration;

    /**
     * JWTトークン作成
     * 
     * @param req HttpServletRequest
     * @param auth Authentication
     * @return JWTトークン
     */
    public String createToken(HttpServletRequest req, Authentication auth) {
        return Jwts.builder()
            .setClaims(this.createClaims(req.getParameterValues("id")[0], auth))
            .setIssuedAt(new Date())
            .setExpiration(this.createExpireTime())
            .signWith(SignatureAlgorithm.HS256, this.configuration.getTokenSecretKey())
            .compact();
    }

    private Claims createClaims(String id, Authentication auth) {
        Claims claims = Jwts.claims().setSubject(id);
        claims.put("username", auth.getName());
        claims.put("role", auth.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(joining("\n  ")));
        return claims;
    }

    private Date createExpireTime() {
        Date expireTime = new Date();
        expireTime.setTime(expireTime.getTime() + this.configuration.getTokenValidDuration());
        return expireTime;
    }
}
