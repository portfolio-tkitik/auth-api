package com.learning.authentication.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import lombok.RequiredArgsConstructor;

@EnableWebSecurity
@RequiredArgsConstructor
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final SimpleAuthenticationEntryPoint authenticationEntryPoint;

    private final SimpleAccessDeniedHandler accessDeniedHandler;

    private final SimpleAuthenticationSuccessHandler successHandler;

    private final SimpleAuthenticationFailureHandler failureHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // 静的リソース(images、css、javascript)に対するアクセスはセキュリティ設定を無視する
        http
            .authorizeRequests()
            .anyRequest()
            .authenticated()
            .and()

            // 例外
            .exceptionHandling()
            .authenticationEntryPoint(this.authenticationEntryPoint)
            .accessDeniedHandler(this.accessDeniedHandler)
            .and()

            // ログイン設定
            .formLogin()
            .loginProcessingUrl("/login").permitAll()
            .usernameParameter("id")
            .passwordParameter("pass")
            .successHandler(this.successHandler)
            .failureHandler(this.failureHandler)
            .and()

            // セッション設定
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()

            // Basic認証設定
            .httpBasic()
            .disable()

            // CSRF設定
            .csrf()
            .disable()

            // CORS設定
            .cors();
    }
}