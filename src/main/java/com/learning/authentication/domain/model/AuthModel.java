package com.learning.authentication.domain.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.learning.authentication.common.Utility.Authority;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 認証情報ドメインモデル。
 */
@RequiredArgsConstructor
@Data
public class AuthModel implements UserDetails{

    /** serialVersionUID  */
    private static final long serialVersionUID = 1L;

    /** id */
    private final String id;

    /** ユーザ名 */
    private final String username;

    /** パスワード */
    private final String password;

    /** メールアドレス */
    private final String email;

    /** 権限 */
    private final Authority authority;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorityList = new ArrayList<>();
        authorityList.add(new SimpleGrantedAuthority(this.authority.toString()));
        return authorityList;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
