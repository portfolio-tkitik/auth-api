package com.learning.authentication.domain.service;

import com.learning.authentication.domain.model.AuthModel;
import com.learning.authentication.domain.repository.AuthRepository;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service("simpleUserDetailsService")
public class SimpleUserDetailsService implements UserDetailsService {

    @NonNull
    private final AuthRepository authRepository;

    /**
     * ユーザ検索。
     *
     * @param id 検索入力値
     * @return 認証情報
     */
    @Override
    public UserDetails loadUserByUsername(String id) {

        // 認証情報
        AuthModel authModel;
            
        // id検索
        authModel = this.authRepository.findById(id);

        // 認証情報設定
        UserDetails userDetails = new User(
            authModel.getUsername(),
            authModel.getPassword(),
            authModel.getAuthorities()
        );

        return userDetails;
    }
}