package com.learning.authentication.domain.repository;

import com.learning.authentication.domain.model.AuthModel;

/**
 * 認証情報を利用するインフラ層とのインターフェース。
 */
public interface AuthRepository {

    /**
     * ユーザ検索。
     *
     * @param id 検索対象のid
     * @return 認証情報
     */
    AuthModel findById(String id);
}