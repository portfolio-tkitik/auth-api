package com.learning.authentication.infrastructure.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.learning.authentication.common.Utility.Authority;

import lombok.Getter;
import lombok.Setter;

/**
 * ユーザ詳細テーブルエンティティクラス。
 */
@Getter
@Setter
@Entity
@Table(name = "user_info")
public class UserInfoEntity implements Serializable {

    /** serialVersionUID  */
    private static final long serialVersionUID = 1L;

    /** id */
    @Id
    @Column(nullable = false, unique = true)
    private String id;

    @OneToOne
    @JoinColumn(name = "id")
    private UserDetailEntity userDetailEntity;

    /** 権限 */
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Authority authority;
}
