package com.learning.authentication.infrastructure.repository;

import com.learning.authentication.infrastructure.entity.UserInfoEntity;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 認証情報を利用するためのインターフェース。
 */
public interface UserInfoJpaRepository extends JpaRepository<UserInfoEntity, String> {
}
